function merge(esquerda, direita) {
    let vetor = new Array()
    while (esquerda.length && direita.length) {

        if (esquerda[0] < direita[0]) {
            vetor.push(esquerda.shift())  
        } else {
            vetor.push(direita.shift()) 
        }
    }
  
    return [...vetor, ...esquerda, ...direita]
}

function mergeSort(vetor) {
    const meio = vetor.length / 2

    if(vetor.length < 2){
      return vetor 
    }
    
    const esquerda = vetor.splice(0, meio)
    return merge(mergeSort(esquerda),mergeSort(vetor))
  }

function geraNumeros(tamanho) {
    let numeros = new Array(tamanho)
    for (i = 0; i < tamanho; i++) {
        numeros[i] = Math.ceil(Math.random() * 100)
    }
    console.log(numeros)
    ordenados = mergeSort(numeros)
    console.log(ordenados)
}

let x = geraNumeros(6)
